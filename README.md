# Initial project - RXJS course

* The first thing to do after downloading the code is to execute the command:

```
npm install
```
This command will download all the node modules needed to run the project.


* When you finish installing the node_modules, then you can run the project with the following command

```
npm start
```
For this to work, remember that you must execute this command in the same directory where the ```package.json```

## Change port
By default, the port I configured for this project is ``8081``, but if you need to change it because that port might be used by your computer, you can change it by opening the ```package.json`` >> scripts. There you will see the instruction that launches the development server

```
"start": "webpack serve --mode development --open --port=8081"
```

Simply change the port to the one you need and you are done (logically save the changes before running ``npm start`` again).


